var firebaseConfig = {
    apiKey: "AIzaSyCR77UIpZhgsNhOe9d1WqMcTlDv4XA_SGU",
    authDomain: "momodoro-6f644.firebaseapp.com",
    databaseURL: "https://momodoro-6f644.firebaseio.com",
    projectId: "momodoro-6f644",
    storageBucket: "momodoro-6f644.appspot.com",
    messagingSenderId: "131107977654",
    appId: "1:131107977654:web:422939c64043c243d76917",
    measurementId: "G-ZEEC30RLD7"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

function getDocumentsOn(day)
{
    const start = (new Date((new Date(day)).toDateString())).getTime()
    const end = start + (24 * 60 * 60 * 1000)

    return new Promise((resolve, reject) =>
    {
        firebase.auth().onAuthStateChanged((user) =>
        {
            if (user)
            {
                console.log("user authenticated")

                const db = firebase.firestore()
                db.collection("users").doc(user.uid).collection("stints")
                    .where("start", ">=", firebase.firestore.Timestamp.fromMillis(start))
                    .where("start", "<=", firebase.firestore.Timestamp.fromMillis(end))
                    .get()
                    .then((querySnapshot) => {
                        console.log("got documents")
                        return resolve(querySnapshot)
                    })
                    .catch((error) => {return reject(error)})
            }
            else
            {
                return reject()
            }
        })
    })
}
